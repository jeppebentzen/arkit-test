#!/bin/sh

echo --------------- GIT AND BUILD v1.0 -------------------------
echo The tool to the developer who really dont want to use a Mac
echo ------------------------------------------------------------
echo Removing local changes...
echo ------------------------------------------------------------

git checkout .
git reset

echo ------------------------------------------------------------
echo Pulling latest changes from git...
echo ------------------------------------------------------------

git pull

echo ------------------------------------------------------------
echo Building Unity...
echo ------------------------------------------------------------

/Applications/Unity/Hub/Editor/2017.3.1f1/Unity.app/Contents/MacOS/Unity -quit -batchmode -projectPath $PWD -logfile buildlog.txt -stackTraceLogType Full -executeMethod AutoBuilder.PerformiOSBuild

echo ------------------------------------------------------------
echo Done...
echo ------------------------------------------------------------