﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ArKitController
{
    public class AdminPanelReferences : MonoBehaviour
    {
        [SerializeField]
        public Slider yAxisSlider;
        [SerializeField]
        public Slider rotationSlider;
        [SerializeField]
        public Button upBtn;
        [SerializeField]
        public Button downBtn;
        [SerializeField]
        public Button rightBtn;
        [SerializeField]
        public Button leftBtn;
        [SerializeField]
        public Button repositionBtn;
        [SerializeField]
        public Button smallerBtn;
        [SerializeField]
        public Button biggerBtn;
        [SerializeField]
        public Button closeBtn;
        [SerializeField]
        public Button openBtn;
        [SerializeField]
        public GameObject pointPlacementBtn;
    }
}


