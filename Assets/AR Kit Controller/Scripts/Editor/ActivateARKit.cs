﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.UI;
using UnityEditor.Events;
namespace ArKitController
{
    public class ActivateARKit
    {

        [MenuItem("Cadpeople/AR Kit/Activate in current scene")]
        private static void ActivateArKit()
        {
            // Load and instantiate prefabs
            var arKitControllerPrefab = (GameObject)AssetDatabase.LoadAssetAtPath("Assets/AR Kit Controller/Prefabs/ARKit Controller.prefab", typeof(GameObject));
            var arKitController = GameObject.Instantiate(arKitControllerPrefab, Vector3.zero, Quaternion.identity);
            arKitController.name = arKitController.name.Replace("(Clone)", "");

            var arCamerasPrefab = (GameObject)AssetDatabase.LoadAssetAtPath("Assets/AR Kit Controller/Prefabs/AR Cameras.prefab", typeof(GameObject));
            var arCameras = GameObject.Instantiate(arCamerasPrefab, Vector3.zero, Quaternion.identity);
            arCameras.name = arCameras.name.Replace("(Clone)", "");

            var arOriginPrefab = (GameObject)AssetDatabase.LoadAssetAtPath("Assets/AR Kit Controller/Prefabs/AR Origin.prefab", typeof(GameObject));
            var arOrigin = GameObject.Instantiate(arOriginPrefab, Vector3.zero, Quaternion.identity);
            arOrigin.name = arOrigin.name.Replace("(Clone)", "");

            var arKitCanvasPrefab = (GameObject)AssetDatabase.LoadAssetAtPath("Assets/AR Kit Controller/Prefabs/AR Kit Canvas.prefab", typeof(GameObject));
            var arKitCanvas = GameObject.Instantiate(arKitCanvasPrefab, Vector3.zero, Quaternion.identity);
            arKitCanvas.name = arKitCanvas.name.Replace("(Clone)", "");

            // Set references
            arKitController.GetComponent<ARKitController>().ModelTransform = arOrigin.transform;
            arKitController.GetComponent<UnityARCameraManager>().m_camera = arCameras.transform.GetChild(0).GetComponent<Camera>();

            var adminPanelRefs = arKitCanvas.transform.GetChild(0).GetComponent<AdminPanelReferences>();
            UnityEventTools.AddPersistentListener(adminPanelRefs.upBtn.onClick,
                arOrigin.GetComponent<PositionModifier>().Up);
            UnityEventTools.AddPersistentListener(adminPanelRefs.downBtn.onClick,
                arOrigin.GetComponent<PositionModifier>().Down);
            UnityEventTools.AddPersistentListener(adminPanelRefs.rightBtn.onClick,
                arOrigin.GetComponent<PositionModifier>().Right);
            UnityEventTools.AddPersistentListener(adminPanelRefs.leftBtn.onClick,
                arOrigin.GetComponent<PositionModifier>().Left);
            UnityEventTools.AddPersistentListener(adminPanelRefs.repositionBtn.onClick,
                arKitController.GetComponent<ARKitController>().StartPlacingModel);
            UnityEventTools.AddPersistentListener(adminPanelRefs.biggerBtn.onClick,
                arCameras.GetComponent<CamTransformer>().MakeBigger);
            UnityEventTools.AddPersistentListener(adminPanelRefs.smallerBtn.onClick,
                arCameras.GetComponent<CamTransformer>().MakeSmaller);
            UnityEventTools.AddPersistentListener(adminPanelRefs.yAxisSlider.onValueChanged,
                arOrigin.GetComponent<HeightModifier>().SetYAxis);
            UnityEventTools.AddPersistentListener(adminPanelRefs.rotationSlider.onValueChanged,
                arOrigin.GetComponent<RotationModifier>().SetRotation);
            UnityEventTools.AddPersistentListener(arKitController.GetComponent<ARKitController>().OnModelPlacedEvent,
                adminPanelRefs.yAxisSlider.GetComponent<ResetValue>().ResetSliderValue);
            UnityEventTools.AddPersistentListener(arKitController.GetComponent<ARKitController>().OnModelPlacedEvent,
                adminPanelRefs.rotationSlider.GetComponent<ResetValue>().ResetSliderValue);
            UnityEventTools.AddPersistentListener(adminPanelRefs.openBtn.onClick,
                arKitController.GetComponent<ARKitController>().OnOpenAdminPanel);
            UnityEventTools.AddPersistentListener(adminPanelRefs.closeBtn.onClick,
                arKitController.GetComponent<ARKitController>().OnCloseAdminPanel);
            UnityEventTools.AddPersistentListener(arKitController.GetComponent<ARKitController>().OnModelPlacedEvent,
                arOrigin.GetComponent<HeightModifier>().OnModelPlaced);
            arKitController.GetComponent<ARKitController>().adminPanelRefs = adminPanelRefs;
            UnityEventTools.AddPersistentListener(adminPanelRefs.pointPlacementBtn.GetComponent<Button>().onClick,
                arKitController.GetComponent<ARKitController>().OnVerifyPointPlacement);
            
            var infoWindow = new ArKitInfoWindow();
            infoWindow.Init();

            Debug.Log("AR Kit activated in current scene");
        }
    }

    public class ArKitInfoWindow : EditorWindow
    {
        public void Init()
        {
            ArKitInfoWindow window = (ArKitInfoWindow)EditorWindow.GetWindow(typeof(ArKitInfoWindow));
            window.Show();
        }

        void OnGUI()
        {
            EditorStyles.label.wordWrap = true;
            GUILayout.Label("AR Kit controller", EditorStyles.boldLabel);

            GUILayout.Space(20);

            EditorGUILayout.LabelField("AR Kit is now activated in the scene. Place the content you want in AR as a child of AR Origin."
                + " If you are using point placement you need to create a new layer to place the prefab \"Ground pointer\" (AR Kit Controller > Prefabs) into"
                + " . The AR Camera should only render this layer.");

        }

    }

}
