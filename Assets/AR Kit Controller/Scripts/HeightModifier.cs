﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace ArKitController
{
    public class HeightModifier : MonoBehaviour
    {

        [SerializeField]
        private float minY;
        [SerializeField]
        private float maxY;

        private float startHeight;

        private void Awake()
        {
            startHeight = transform.localPosition.y;
        }

        public void SetYAxis(float v)
        {
            transform.localPosition = new Vector3(transform.localPosition.x,
                startHeight + (minY + (v * (maxY - minY))), transform.localPosition.z);
        }

        public void OnModelPlaced()
        {
            startHeight = transform.localPosition.y;
        }
    }
}
