﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.iOS;
using TMPro;
using UnityEngine.UI;
using UnityEngine.Events;

namespace ArKitController
{
#if UNITY_EDITOR
    using UnityEditor;

    [CustomEditor(typeof(ARKitController))]
    public class ARKitControllerEditor : Editor
    {
        ARKitController controller;

        private void Awake()
        {
            controller = (ARKitController)target;
        }

        public override void OnInspectorGUI()
        {
            GUI.changed = false;
            EditorStyles.label.wordWrap = true;

            // ------------- Title --------------------
            GUILayout.Space(20);
            EditorStyles.boldLabel.alignment = TextAnchor.MiddleCenter;
            EditorGUILayout.LabelField("AR Kit Settings", EditorStyles.boldLabel);
            EditorStyles.boldLabel.alignment = TextAnchor.MiddleLeft;
            GUILayout.Space(20);
            // ------------- SHOW PLANES ---------------
            GUILayout.Space(20);
            EditorGUILayout.LabelField("Enable AR Planes to be created on found surfaces to visualize where you can place models");

            controller.ShowPlanes = GUILayout.Toggle(controller.ShowPlanes, "   Show planes");
            if (controller.ShowPlanes)
            {
                UnityARGeneratePlane planeGenerator = controller.GetComponent<UnityARGeneratePlane>();
                if(planeGenerator == null)
                {
                    planeGenerator = controller.gameObject.AddComponent<UnityARGeneratePlane>();
                }
                planeGenerator.enabled = true;
                planeGenerator.planePrefab = (GameObject)EditorGUILayout.ObjectField("Plane prefab",
                        planeGenerator.planePrefab, typeof(GameObject), false);
            } else if (controller.GetComponent<UnityARGeneratePlane>() != null)
            {
                controller.GetComponent<UnityARGeneratePlane>().enabled = false;
            }
            GUILayout.Space(20);

            // ------------- Instantiate or translate ---------------
            GUILayout.Space(20);
            EditorGUILayout.LabelField("Instantiate a prefab when plane is found or just translate an already existing model");

            controller.InstantiateModel = GUILayout.Toggle(controller.InstantiateModel, "   Instantiate model");
            if (controller.InstantiateModel)
            {
                controller.ModelPrefab = (GameObject)EditorGUILayout.ObjectField("Model prefab", controller.ModelPrefab, typeof(GameObject), false);
            } else
            {
                controller.ModelTransform = (Transform)EditorGUILayout.ObjectField("Model transform", controller.ModelTransform, typeof(Transform), true);
            }

            GUILayout.Space(20);
            EditorGUILayout.LabelField("Choose tracking type:");
            
            controller.ARPlacementType = (ARKitController.PlacementType)EditorGUILayout.EnumPopup(controller.ARPlacementType);

            switch (controller.ARPlacementType)
            {
                case ARKitController.PlacementType.ImageTracking:
                    EditorGUILayout.LabelField("Model is now placed automatically on the reference image.");
                    controller.ReferenceImage = (ARReferenceImage)EditorGUILayout.ObjectField("Reference image", controller.ReferenceImage, typeof(ARReferenceImage), true);
                    break;
                case ARKitController.PlacementType.PressPlacement:
                    EditorGUILayout.LabelField("Placing the model on the ground is now done by pressing where on the ground you want it to be placed. You can reposition the model through admin panel.");
                    break;
                case ARKitController.PlacementType.LookPlacement:
                    EditorGUILayout.LabelField("Placing the model on the ground is now done by pointing the camera at the ground and pressing the screen when placing. Please set point prefab.");
                    controller.PointPrefab = (GameObject)EditorGUILayout.ObjectField("Point prefab", controller.PointPrefab, typeof(GameObject), false);
                    break;
                default:
                    break;
            }
            
            GUILayout.Space(20);
            if (GUI.changed)
                EditorUtility.SetDirty(controller);
        }
    }
#endif

    public class ARKitController : MonoBehaviour
    {
        public enum PlacementType { ImageTracking, PressPlacement, LookPlacement }
        [SerializeField]
        private bool showPlanes;
        [SerializeField]
        private Transform modelTransform;
        [SerializeField]
        private bool instantiateModel;
        [SerializeField]
        private GameObject modelPrefab;
        [SerializeField]
        private GameObject pointPrefab;
        [SerializeField]
        private PlacementType placementType;
        [SerializeField]
        private ARReferenceImage referenceImage;
        private Button closeARKitButton;
        private Button repositionButton;
        private GameObject groundPointer;
        private Transform debugPlanesParent;
        #region Properties
        public bool IsPlacingModel { get; private set; }
        public bool HasFoundPlane { get; private set; }
        public Transform ModelTransform
        {
            get { return modelTransform; }
            set { modelTransform = value; }
        }
        public Action OnPlaneFound { get; set; }
        public Action OnModelPlaced { get; set; }
        public UnityEvent OnModelPlacedEvent;
        public bool ShowPlanes { get { return showPlanes; } set { showPlanes = value; } }
        public bool InstantiateModel { get { return instantiateModel; } set { instantiateModel = value; } }
        public GameObject PointPrefab { get { return pointPrefab; } set { pointPrefab = value; } }
        public GameObject ModelPrefab { get { return modelPrefab; } set { modelPrefab = value; } }
        public PlacementType ARPlacementType { get { return placementType; } set { placementType = value; } }
        public ARReferenceImage ReferenceImage { get { return referenceImage; } set { referenceImage = value; } }
        public AdminPanelReferences adminPanelRefs;
        #endregion

        // Singleton
        private static ARKitController instance;

        public static ARKitController Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = FindObjectOfType(typeof(ARKitController)) as ARKitController;
                    if (instance == null)
                    {
                        Debug.LogError("There is no ARKit controller in the scene!");
                        return null;
                    }
                }
                return instance;
            }
        }

        private void Start()
        {
            if(ARPlacementType == PlacementType.ImageTracking)
            {
                UnityARSessionNativeInterface.ARImageAnchorAddedEvent += AddImageAnchor;
                UnityARSessionNativeInterface.ARImageAnchorUpdatedEvent += UpdateImageAnchor;
                UnityARSessionNativeInterface.ARImageAnchorRemovedEvent += RemoveImageAnchor;
            } else
            {
                UnityARSessionNativeInterface.ARFrameUpdatedEvent += OnARFrameUpdate;
            }
            closeARKitButton = adminPanelRefs.closeBtn;
            repositionButton = adminPanelRefs.repositionBtn;
            debugPlanesParent = transform.Find("DebugPlanesParent");
        }

        void OnDestroy()
        {
            if (ARPlacementType == PlacementType.ImageTracking)
            {
                UnityARSessionNativeInterface.ARImageAnchorAddedEvent -= AddImageAnchor;
                UnityARSessionNativeInterface.ARImageAnchorUpdatedEvent -= UpdateImageAnchor;
                UnityARSessionNativeInterface.ARImageAnchorRemovedEvent -= RemoveImageAnchor;
            }
            else
            {
                UnityARSessionNativeInterface.ARFrameUpdatedEvent -= OnARFrameUpdate;
            }
        }

        public void ChangeTrackingType()
        {
            // Unsubscribe to events
            if(ARPlacementType == PlacementType.ImageTracking)
            {
                RemoveImageAnchor(null);
                UnityARSessionNativeInterface.ARImageAnchorAddedEvent -= AddImageAnchor;
                UnityARSessionNativeInterface.ARImageAnchorUpdatedEvent -= UpdateImageAnchor;
                UnityARSessionNativeInterface.ARImageAnchorRemovedEvent -= RemoveImageAnchor;
            } else
            {
                UnityARSessionNativeInterface.ARFrameUpdatedEvent -= OnARFrameUpdate;
            }
            // Set new placement type
            ARPlacementType = ARPlacementType == PlacementType.ImageTracking ? 
                PlacementType.LookPlacement : PlacementType.ImageTracking;
            // Subscribe to new Events
            if (ARPlacementType == PlacementType.ImageTracking)
            {
                UnityARSessionNativeInterface.ARImageAnchorAddedEvent += AddImageAnchor;
                UnityARSessionNativeInterface.ARImageAnchorUpdatedEvent += UpdateImageAnchor;
                UnityARSessionNativeInterface.ARImageAnchorRemovedEvent += RemoveImageAnchor;
            }
            else
            {
                UnityARSessionNativeInterface.ARFrameUpdatedEvent += OnARFrameUpdate;
            }
            // Start placing model
            HasFoundPlane = false;
            StartPlacingModel();
        }

        private void AddImageAnchor(ARImageAnchor arImageAnchor)
        {
            // Check if found image is the reference image
            if (arImageAnchor.referenceImageName == ReferenceImage.imageName)
            {
                // Get position and rotation
                Vector3 position = UnityARMatrixOps.GetPosition(arImageAnchor.transform);
                Quaternion rotation = UnityARMatrixOps.GetRotation(arImageAnchor.transform);

                // Instantiate or place model
                InstantiateOrPlaceModel(position, rotation);

                // Trigger events
                if (OnModelPlaced != null)
                    OnModelPlaced();
                if (OnModelPlacedEvent != null)
                    OnModelPlacedEvent.Invoke();
            }
        }

        private void UpdateImageAnchor(ARImageAnchor arImageAnchor)
        {
            // Check if found image is the reference image
            if (ModelTransform != null && arImageAnchor.referenceImageName == referenceImage.imageName)
            {
                var position = UnityARMatrixOps.GetPosition(arImageAnchor.transform);
                var rotation = UnityARMatrixOps.GetRotation(arImageAnchor.transform);
                // Update position and rotation
                ModelTransform.position = position;
                ModelTransform.rotation = rotation;
            }
        }

        private void RemoveImageAnchor(ARImageAnchor arImageAnchor)
        {
            // Either destroy or move model transform
            if(InstantiateModel && ModelTransform != null)
            {
                Destroy(ModelTransform.gameObject);
            } else if(ModelTransform != null)
            {
                ModelTransform.position = new Vector3(1000f, 1000f, 1000f);
            }
        }

        /// <summary>
        /// Called when AR Frame is updated
        /// </summary>
        /// <param name="arCamera"></param>
        private void OnARFrameUpdate(UnityARCamera arCamera)
        {
            if (!HasFoundPlane && arCamera.pointCloudData.Length > 0)
            {
                StartPlacingModel();
                HasFoundPlane = true;
                // Trigger OnPlaneFound callback
                if (OnPlaneFound != null)
                    OnPlaneFound();
            }
        }

        /// <summary>
        /// Start placing the main model.
        /// </summary>
        /// <param name="param"></param>
        public void StartPlacingModel()
        {
            IsPlacingModel = true;
            if (placementType == PlacementType.ImageTracking)
            {
                RemoveImageAnchor(null);
            }
            if (placementType == PlacementType.LookPlacement)
                adminPanelRefs.pointPlacementBtn.SetActive(true);
            if(ModelTransform != null)
                ModelTransform.position = new Vector3(10000f, 10000f, 10000f);
        }

        public void OnOpenAdminPanel()
        {
            if (debugPlanesParent != null)
            {
                debugPlanesParent.gameObject.SetActive(true);
            }
        }

        public void OnCloseAdminPanel()
        {
            if (debugPlanesParent != null)
            {
                debugPlanesParent.gameObject.SetActive(false);
            }
        }

        private void LateUpdate()
        {
            // If placing model and touch input is registered check for placement of model
            if (placementType == PlacementType.PressPlacement && IsPlacingModel && Input.touchCount > 0)
            {
                CheckTouchPlacement();
            } else if(placementType == PlacementType.LookPlacement && IsPlacingModel)
            {
                CheckPointPlacement();
            }
        }

        /// <summary>
        /// User verifies the position of the model in point placement
        /// </summary>
        public void OnVerifyPointPlacement()
        {
            adminPanelRefs.pointPlacementBtn.SetActive(false);
            IsPlacingModel = false;

            // Instantiate or place model
            InstantiateOrPlaceModel(groundPointer.transform.position, groundPointer.transform.rotation);

            // Set interactable back on
            if (closeARKitButton != null)
                closeARKitButton.interactable = true;
            if (repositionButton != null)
                repositionButton.interactable = true;

            // Trigger callbacks
            if (OnModelPlaced != null)
                OnModelPlaced();
            if (OnModelPlacedEvent != null)
                OnModelPlacedEvent.Invoke();

            // Destroy ground pointer
            if (groundPointer != null)
                Destroy(groundPointer);
        }

        /// <summary>
        /// Checks if the model can be placed where the camera is pointing
        /// </summary>
        private void CheckPointPlacement()
        {
            // Get center of screen as an AR point
            var centerPosition = Camera.main.ScreenToViewportPoint(
                new Vector2(
                    Screen.width * 0.5f,
                    Screen.height * 0.5f)
                );
            var centerPoint = new ARPoint
            {
                x = centerPosition.x,
                y = centerPosition.y
            };
            // AR result types setup
            ARHitTestResultType[] resultTypes = {
                ARHitTestResultType.ARHitTestResultTypeExistingPlaneUsingExtent,
                ARHitTestResultType.ARHitTestResultTypeEstimatedHorizontalPlane,
                ARHitTestResultType.ARHitTestResultTypeEstimatedVerticalPlane,
                ARHitTestResultType.ARHitTestResultTypeFeaturePoint
                };
            Vector3 pos;
            Quaternion rot;
            // Check each resulttype
            foreach (ARHitTestResultType resultType in resultTypes)
            {
                if(HitTestWithResultType(centerPoint, resultType, out pos, out rot))
                {
                    if(groundPointer == null)
                    {
                        groundPointer = Instantiate(pointPrefab);
                    }
                    groundPointer.transform.position = pos;
                    groundPointer.transform.rotation = rot;
                    return;
                }
            }
            if (groundPointer != null)
                groundPointer.transform.position = new Vector3(1000f, 1000f, 1000f);
        }

        /// <summary>
        /// Checks if the model can be placed on the touched spot
        /// </summary>
        private void CheckTouchPlacement()
        {
            var touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began || touch.phase == TouchPhase.Moved)
            {
                var screenPosition = Camera.main.ScreenToViewportPoint(touch.position);
                ARPoint point = new ARPoint
                {
                    x = screenPosition.x,
                    y = screenPosition.y
                };

                // prioritize reults types
                ARHitTestResultType[] resultTypes = {
                ARHitTestResultType.ARHitTestResultTypeExistingPlaneUsingExtent,
                ARHitTestResultType.ARHitTestResultTypeEstimatedHorizontalPlane,
                ARHitTestResultType.ARHitTestResultTypeFeaturePoint
                };

                foreach (ARHitTestResultType resultType in resultTypes)
                {
                    Vector3 pos;
                    Quaternion rot;
                    if (HitTestWithResultType(point, resultType, out pos, out rot))
                    {
                        IsPlacingModel = false;

                        // Instantiate or place model
                        InstantiateOrPlaceModel(pos, rot);

                        // Set interactable back on
                        if (closeARKitButton != null)
                            closeARKitButton.interactable = true;
                        if (repositionButton != null)
                            repositionButton.interactable = true;

                        // Trigger callbacks
                        if (OnModelPlaced != null)
                            OnModelPlaced();
                        if (OnModelPlacedEvent != null)
                            OnModelPlacedEvent.Invoke();
                        
                        
                        return;
                    }
                }
            }
        }

        /// <summary>
        /// Tries to move HitTransform to a valid position on a AR plane.
        /// </summary>
        /// <param name="point">The point in screen position</param>
        /// <param name="resultTypes">Filter the result based on resultType</param>
        /// <returns></returns>
        private bool HitTestWithResultType(ARPoint point, ARHitTestResultType resultTypes, out Vector3 position, out Quaternion rotation)
        {
            List<ARHitTestResult> hitResults = UnityARSessionNativeInterface.GetARSessionNativeInterface().HitTest(point, resultTypes);
            if (hitResults.Count > 0)
            {
                foreach (var hitResult in hitResults)
                {
                    position = UnityARMatrixOps.GetPosition(hitResult.worldTransform);
                    rotation = UnityARMatrixOps.GetRotation(hitResult.worldTransform);
                    return true;
                }
            }
            position = Vector3.zero;
            rotation = Quaternion.identity;
            return false;
        }

        private void InstantiateOrPlaceModel(Vector3 pos, Quaternion rot)
        {
            // If instantiate -> Destroy and instantiate new model
            if (InstantiateModel)
            {
                // Destroy and instantiate
                if (ModelTransform != null)
                {
                    Destroy(ModelTransform.gameObject);
                }
                var newModel = Instantiate(ModelPrefab);
                ModelTransform = newModel.transform;
            }
            // Set the position and rotation
            ModelTransform.position = pos;
            ModelTransform.rotation = rot;
        }
    }
}
