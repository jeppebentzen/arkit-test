﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ArKitController
{
    public class ActivateOrDeactivate : MonoBehaviour
    {

        public void RevertActivation()
        {
            gameObject.SetActive(!gameObject.activeSelf);
        }

    }
}

