﻿

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.iOS;
using TMPro;
namespace ArKitController
{
    public class CamTransformer : MonoBehaviour
    {
        [SerializeField]
        private Camera normalCamera;
        [SerializeField]
        private Camera arCamera;
        [SerializeField]
        private float cameraScale;
        [SerializeField]
        private float scaleStep;

        void LateUpdate()
        {
            Matrix4x4 matrix = UnityARSessionNativeInterface.GetARSessionNativeInterface().GetCameraPose();
            float invScale = 1.0f / cameraScale;
            Vector3 cameraPos = UnityARMatrixOps.GetPosition(matrix);
            Vector3 vecAnchorToCamera = cameraPos - ARKitController.Instance.ModelTransform.position;
            normalCamera.transform.localPosition = ARKitController.Instance.ModelTransform.position + (vecAnchorToCamera * invScale);
            normalCamera.transform.localRotation = UnityARMatrixOps.GetRotation(matrix);

            //this needs to be adjusted for near/far
            normalCamera.projectionMatrix = UnityARSessionNativeInterface.GetARSessionNativeInterface().GetCameraProjection();
        }

        public void MakeSmaller()
        {
            cameraScale -= scaleStep;
        }

        public void MakeBigger()
        {
            cameraScale += scaleStep;
        }

        public void SetCameraScale(float scale)
        {
            cameraScale = scale;
        }
    }
}
