﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace ArKitController
{
    public class RotationModifier : MonoBehaviour
    {

        public void SetRotation(float v)
        {
            transform.rotation = Quaternion.AngleAxis(v, Vector3.up);
        }
    }
}
