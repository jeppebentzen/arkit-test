﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace ArKitController
{
    public class PositionModifier : MonoBehaviour
    {

        public float stepSize;

        public void SetPositionX(float x)
        {
            transform.position = new Vector3(x, transform.position.y, transform.position.z);
        }

        public void SetPositionZ(float z)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, z);
        }

        public void Up()
        {
            transform.position += transform.forward * stepSize;
        }
        public void Down()
        {
            transform.position -= transform.forward * stepSize;
        }
        public void Right()
        {
            transform.position += transform.right * stepSize;
        }
        public void Left()
        {
            transform.position -= transform.right * stepSize;
        }
    }
}
