﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ResetValue : MonoBehaviour {
    [SerializeField]
    private float startValue;

    private void Awake()
    {
        startValue = GetComponent<Slider>().value;
    }

    public void ResetSliderValue()
    {
        GetComponent<Slider>().value = startValue;
    }
}
